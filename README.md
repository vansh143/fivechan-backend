## How to use

With docker (local mongo)
    docker-compose build
    docker-compose up
Using without docker (mongo atlas)
    npm start


# User Api

# Endpoints

# /login

    body:{
        
        email:string,
        password:string
    }

# /signup

    body:{
        name:string,
        email:string,
        password:string,
    }

# /home

    headers.authorisation[
        "${jwtToken}"
    ]